package com.bldev.carsuite.interactor

import com.bldev.carsuite.io.ApiServiceGenerator
import com.bldev.carsuite.io.GovernmentDataService
import com.bldev.carsuite.io.OnFoundCarriagePlateListener
import com.bldev.carsuite.model.AutomobileInfo
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

/**
 * Created by leonelmendez on 29/08/15.
 */

class CarriagePlateInteractorImpl : CarriagePlateInteractor {


    override fun searchCarriagePlate(carriagePlate: String, carriagePlateCallback: OnFoundCarriagePlateListener) {
       val apiService = ApiServiceGenerator.createService(javaClass<GovernmentDataService>(),"http://api.labcd.mx/v1");

        apiService.automobileData(carriagePlate, object : Callback<AutomobileInfo> {

            override fun success(automobile: AutomobileInfo?, response: Response?) {
                carriagePlateCallback.onFoundCarriagePlate(automobile)
            }

            override fun failure(error: RetrofitError?) {
                error?.printStackTrace()
                carriagePlateCallback.onNotFoundCarriagePlate(error)
            }

        })
    }
}


