package com.bldev.carsuite.interactor

import com.bldev.carsuite.io.OnFoundCarriagePlateListener

/**
 * Created by leonelmendez on 29/08/15.
 */

interface CarriagePlateInteractor {
    fun searchCarriagePlate(carriagePlate:String,carriagePlateCallback:OnFoundCarriagePlateListener)
}
