package com.bldev.carsuite.model

import java.util.HashMap


public class Infraction {

    public var id: Int? = null
    public var folio: String? = null
    public var fecha: String? = null
    public var situacion: String? = null
    public var motivo: String? = null
    public var fundamento: String? = null
    public var sancion: String? = null
    public var createdAt: String? = null
    public var updatedAt: String? = null
    public var idVehicle: Int? = null
    private val additionalProperties = HashMap<String, Any>()

    public fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    public fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

}
