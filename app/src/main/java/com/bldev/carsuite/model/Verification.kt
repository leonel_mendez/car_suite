package com.bldev.carsuite.model


import java.util.HashMap


public data class Verification {

    public var id: Int? = null
    public var vin: String? = null
    public var marca: String? = null
    public var submarca: String? = null
    public var modelo: String? = null
    public var combustible: String? = null
    public var certificado: String? = null
    public var cancelado: String? = null
    public var vigencia: String? = null
    public var verificentro: String? = null
    public var linea: String? = null
    public var fechaVerificacion: String? = null
    public var horaVerificacion: String? = null
    public var resultado: String? = null
    public var causaRechazo: String? = null
    public var equipoGdf: String? = null
    public var createdAt: String? = null
    public var updatedAt: String? = null
    public var idVehicle: Int? = null
    private val additionalProperties = HashMap<String, Any>()

    public fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    public fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

}
