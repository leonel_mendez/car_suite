package com.bldev.carsuite.model

import java.io.Serializable
import java.util.ArrayList
import java.util.HashMap

public class AutomobileInfo:Serializable {

    public var vehicle: Vehicle? = null
    public var verifications: List<Verification> = ArrayList()
    public var infractions: List<Infraction> = ArrayList()
    private val additionalProperties = HashMap<String, Any>()

    public fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    public fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

}
