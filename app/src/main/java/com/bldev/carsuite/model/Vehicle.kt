package com.bldev.carsuite.model

/**
 * Created by leonelmendez on 29/08/15.
 */
import java.util.HashMap

public class Vehicle {

    public var id: Int? = null
    public var placa: String? = null
    public var fechasAdeudoTenecia: String? = null
    public var tieneAdeudoTenencia: String? = null
    public var createdAt: String? = null
    public var updatedAt: String? = null
    private val additionalProperties = HashMap<String, Any>()

    public fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    public fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

}
