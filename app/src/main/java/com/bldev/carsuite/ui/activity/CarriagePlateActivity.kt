package com.bldev.carsuite.ui.activity

import com.bldev.carsuite.R
import com.bldev.carsuite.ui.common.BaseActivity

class CarriagePlateActivity:BaseActivity(){

    override fun resourceLayout(): Int {
        return R.layout.activity_carriage_plate
    }
}