package com.bldev.carsuite.ui.fragment

import com.bldev.carsuite.ui.common.BaseFragment
import com.bldev.carsuite.viewmodel.CarDebtsView
import com.bldev.carsuite.viewmodel.CarInfoView

/**
 * Created by leonelmendez on 11/09/15.
 */
class CarDebtsFragment:BaseFragment(),CarDebtsView{

    override fun resourceLayout(): Int {
        throw UnsupportedOperationException()
    }

    override fun hideLoadingDataProgress() {
        throw UnsupportedOperationException()
    }

    override fun showErrorMessage() {
        throw UnsupportedOperationException()
    }

    override fun showLoadingDataProgress() {
        throw UnsupportedOperationException()
    }



}