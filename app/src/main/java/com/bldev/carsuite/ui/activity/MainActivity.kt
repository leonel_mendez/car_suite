package com.bldev.carsuite.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.bldev.carsuite.R
import com.bldev.carsuite.ui.common.BaseActivity

public class MainActivity : BaseActivity() {

    override fun resourceLayout(): Int {
        return R.layout.activity_main
    }
}
