package com.bldev.carsuite.ui.util

import android.app.ProgressDialog
import android.graphics.Color
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.bldev.carsuite.R

/**
 * Created by leonelmendez on 29/08/15.
 */


fun Fragment.showSnackBar(view: View?, message:String,duration:Int){
    return Snackbar.make(view,message,duration).show()
}

fun Fragment.showSnackBarWithDifferentStyle(view:View?,message:String,duration:Int,backgroundColor:Int,textColor:Int): Snackbar? {

    val snackBar = Snackbar.make(view,message,duration);
    snackBar
            .getView()
            .setBackgroundColor(ContextCompat.getColor(getContext(),backgroundColor))
    (snackBar.getView().findViewById(android.support.design.R.id.snackbar_text) as TextView)
            .setTextColor(textColor)
    snackBar.show()
    return snackBar
}

fun Fragment.showCarsuiteSnackBar(view:View?,message:String, duration:Int= Snackbar.LENGTH_SHORT){
    this.showSnackBarWithDifferentStyle(view,message,duration,R.color.primary_dark,Color.WHITE)
}


fun Fragment.view():View{
    return this.getView()
}

fun EditText.getContent():String{
    return this.getText().toString()
}

fun EditText.isEmpty():Boolean{
    return TextUtils.isEmpty(this.getContent())
}

