package com.bldev.carsuite.ui.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import com.bldev.carsuite.R
import com.bldev.carsuite.interactor.CarriagePlateInteractorImpl
import com.bldev.carsuite.presenter.CarriagePlatePresenter
import com.bldev.carsuite.ui.common.BaseFragment
import com.bldev.carsuite.ui.util.getContent
import com.bldev.carsuite.ui.util.isEmpty
import com.bldev.carsuite.ui.util.showCarsuiteSnackBar
import com.bldev.carsuite.ui.util.view
import com.bldev.carsuite.viewmodel.CarriagePlateView
import kotlinx.android.synthetic.fragment_carriage_plate.view.carriage_plate_edit
import kotlinx.android.synthetic.fragment_carriage_plate.view.search_carriage_plate_btn


class CarriagePlateFragment():BaseFragment(),CarriagePlateView{


    private var carriagePlatePresenter:CarriagePlatePresenter? = null
    private var searchingProgress:ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BaseFragment>.onCreate(savedInstanceState)

        carriagePlatePresenter = CarriagePlatePresenter(this,CarriagePlateInteractorImpl())
    }

    override fun resourceLayout(): Int {
        return R.layout.fragment_carriage_plate
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super<BaseFragment>.onViewCreated(view, savedInstanceState)

        view!!.search_carriage_plate_btn.setOnClickListener {
            getCarriagePlate()
        }
    }

    override fun showLoadingDataProgress() {
        searchingProgress = ProgressDialog.show(this.getContext(),"",getString(R.string.searching_carriage_plate_message))
    }

    override fun hideLoadingDataProgress() {
        searchingProgress?.hide()
    }

    override fun showErrorMessage() {
        showCarsuiteSnackBar(view(),getString(R.string.searching_error_message))
    }

    private fun getCarriagePlate() {
        if(view().carriage_plate_edit.isEmpty()){
            showCarsuiteSnackBar(view(),getString(R.string.no_empty_carriage_plate))
        }else{
            carriagePlatePresenter?.searchCarriagePlate(view().carriage_plate_edit.getContent())
        }
    }

}