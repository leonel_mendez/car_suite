package com.bldev.carsuite.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bldev.carsuite.R
import com.bldev.carsuite.ui.common.BaseFragment
import com.bldev.carsuite.viewmodel.CarInfoView
import com.bldev.carsuite.viewmodel.CarView


class CarInfoFragment : BaseFragment(), CarInfoView {

    override fun resourceLayout(): Int {
        return R.layout.fragment_car_info
    }

    override fun showErrorMessage() {
        throw UnsupportedOperationException()
    }

    override fun showLoadingDataProgress() {
        throw UnsupportedOperationException()
    }

    override fun hideLoadingDataProgress() {
        throw UnsupportedOperationException()
    }

}