package com.bldev.carsuite.io

import com.squareup.okhttp.OkHttpClient
import retrofit.RestAdapter
import retrofit.client.OkClient


/**
 * Created by leonelmendez on 29/08/15.
 */

class ApiServiceGenerator private constructor(){

    companion object ServiceFactory{

        fun createService<Any>(serviceClass:Class<Any>,baseUrl:String):Any{
            val restAdapterBuilder = RestAdapter.Builder()
                    .setEndpoint(baseUrl)
                    .setClient(OkClient(OkHttpClient())).build()

                return restAdapterBuilder.create(serviceClass)
        }
    }
}