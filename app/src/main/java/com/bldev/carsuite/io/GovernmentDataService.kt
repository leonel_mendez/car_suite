package com.bldev.carsuite.io

import com.bldev.carsuite.model.AutomobileInfo
import retrofit.Callback
import retrofit.http.GET
import retrofit.http.Path


interface GovernmentDataService{
    @GET("/movilidad/vehiculos/{carriage}")
    fun automobileData(@Path("carriage")carriage:String,callback:Callback<AutomobileInfo>)
}