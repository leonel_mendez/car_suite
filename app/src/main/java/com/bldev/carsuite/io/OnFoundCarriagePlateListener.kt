package com.bldev.carsuite.io

import com.bldev.carsuite.model.AutomobileInfo
import retrofit.RetrofitError

/**
 * Created by leonelmendez on 29/08/15.
 */

interface OnFoundCarriagePlateListener{
    fun onFoundCarriagePlate(automobile:AutomobileInfo?)
    fun onNotFoundCarriagePlate(error: RetrofitError?)
}