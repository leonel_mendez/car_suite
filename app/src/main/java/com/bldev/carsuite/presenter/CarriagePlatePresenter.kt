package com.bldev.carsuite.presenter

import android.content.Intent
import android.util.Log
import com.bldev.carsuite.interactor.CarriagePlateInteractor
import com.bldev.carsuite.io.OnFoundCarriagePlateListener
import com.bldev.carsuite.model.AutomobileInfo
import com.bldev.carsuite.viewmodel.CarriagePlateView
import retrofit.RetrofitError

/**
 * Created by leonelmendez on 29/08/15.
 */

class CarriagePlatePresenter(val carriagePlateView:CarriagePlateView,val carriagePlateInteractor:CarriagePlateInteractor)
:Presenter,OnFoundCarriagePlateListener{

    public fun searchCarriagePlate(carriagePlate:String ){
        carriagePlateView.showLoadingDataProgress()
        carriagePlateInteractor.searchCarriagePlate(carriagePlate,this)
    }

    override fun onFoundCarriagePlate(automobile:AutomobileInfo?) {
        carriagePlateView.hideLoadingDataProgress()
        Log.d("CarData",automobile.toString())
    }

    override fun onNotFoundCarriagePlate(error:RetrofitError?) {
        carriagePlateView.hideLoadingDataProgress()
        carriagePlateView.showErrorMessage()
    }
}