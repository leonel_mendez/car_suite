package com.bldev.carsuite.viewmodel

interface CarView{

    fun showLoadingDataProgress()
    fun hideLoadingDataProgress()
    fun showErrorMessage()
}
